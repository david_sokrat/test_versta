$(document).ready(function() {
    /* Dummy data to emulate req/res model */

    const googleSearchArticles = [
        {
            title: 'Coast Guard Searches for Missing Royal...',
            type: 'trand',
            channel: 'ABC News',
            date: new Date(),
            image: '../img/coast_guard.png',
            body: `Royal Caribbean's Navigator of the Seas pulls out of New Orleans on the Mississippi River, Feb. 2, 2013. 76 Shares. Email. Coast Guard crews ...`
        }, {
            title: '​Lena Dunham hospitalized after ovarian...',
            type: 'trand',
            channel: 'CBS News',
            date: new Date(),
            image: '../img/lena_dunham.png',
            body: `Lena Dunham will undergo surgery after experiencing an ovarian cyst rupture. Spokeswoman Cindi Berger said in a statement`
        }, {
            title: 'Ben Carson suspends his 2016 presidential...',
            type: 'trand',
            channel: 'CBS News',
            date: new Date(),
            image: '../img/ben_carson.png',
            body: `Politics. Donald Trump to skip conservative summit. By Stephanie Condon CBS News March 4, 2016, 5:48 PMCarson Spent Heavily on Consultants, Lightly on...`
        }, {
            title: 'Google News',
            type: 'top',
            channel: 'Google News',
            site: 'news.google.com',
            body: `Comprehensive up-to-date news coverage, aggregated from sources all over the world by Google News.`
        }, {
            title: 'Fox News - Breaking News Updates | Latest News ...',
            type: 'top',
            channel: 'Fox News Channel',
            site: 'foxnews.com',
            body: `Offers worldwide news coverage, analysis, show profiles, broadcast schedules, team biographies, and email news alerts.`
        }, {
            title: 'CNN - Breaking News, Latest News and Videos',
            type: 'top',
            channel: 'CNN',
            site: 'cnn.com',
            body: `View the latest news and breaking news today for U.S., world, weather, entertainment, politics and health at CNN.com.`
        }, {
            title: 'Yahoo News - Latest News & Headlines',
            type: 'top',
            channel: 'Yahoo! News',
            site: 'yahoo.com',
            body: `The latest news and headlines from Yahoo! News. Get breaking news stories and in-depth coverage with videos and photos.`
        }
    ];

    for (var i = 0; i < googleSearchArticles.length; i++) {
        var a = document.createElement('a');
        var titleText = document.createTextNode(googleSearchArticles[i].title);

        a.setAttribute('href', '#');
        a.setAttribute('class', 'article_title');
    
        var span = document.createElement('span');
        var bodyText = document.createTextNode(googleSearchArticles[i].body);
        
        span.setAttribute('class', 'article_body');
        a.append(titleText);
        span.append(bodyText);

        if (googleSearchArticles[i].type == 'trand') {
            $('.trending_articles_container').append(a);
            $('.trending_articles_container').append(span);
        }

        if (googleSearchArticles[i].type == 'top') {
            a.setAttribute('href', googleSearchArticles[i].site);
            $('.top_articles_container').append(a);
            $('.top_articles_container').append(span);
        }
    }
});